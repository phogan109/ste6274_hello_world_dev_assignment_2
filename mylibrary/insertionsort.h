#ifndef INSERTIONSORT_H
#define INSERTIONSORT_H

#include <algorithm>

namespace mylib {

    template <class RandomAccessIterator, class Compare = std::less<typename RandomAccessIterator::value_type>>
    void insertion_sort(RandomAccessIterator first, RandomAccessIterator last, Compare cmp = Compare())
    {
        using size_type = typename RandomAccessIterator::difference_type;
        size_type n {last - first};
        size_type j;
        size_type x;

        for (size_type i {0}; i < n; ++i)
        {
            x = first[i];
            j = i;
            for ( ; j > 0 && x < first[j-1]; j--)
            {
                first[j] = first[j-1];
            }
            first[j] = x;
        }
    }

/*

    template <typename RandomAccessIterator, typename Predicate>
    void insertion_sort2(RandomAccessIterator begin, RandomAccessIterator end,
                        Predicate p) {
      for (auto i = begin; i != end; ++i) {
        std::rotate(std::upper_bound(begin, i, *i, p), i, i + 1);
      }
    }

    template <typename RandomAccessIterator>
    void insertion_sort1(RandomAccessIterator begin, RandomAccessIterator end) {
      insertion_sort2(
          begin, end,
          std::less<
              typename std::iterator_traits<RandomAccessIterator>::value_type>());
    }
    */

}

#endif // INSERTIONSORT_H

