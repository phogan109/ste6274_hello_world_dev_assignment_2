#ifndef INTROSORT_H
#define INTROSORT_H

#include <math.h>
#include <algorithm>
#include "heapsort.h"
#include "insertionsort.h"
#include <functional>



namespace mylib {


    namespace mylibprivate
    {

        template <class RandomAccessIterator, class Compare = std::greater_equal<typename RandomAccessIterator::value_type>>
        RandomAccessIterator medianOfThree(RandomAccessIterator first, RandomAccessIterator last, RandomAccessIterator middle)
        {
            if ( first >= last && first <= middle || first <= last && first >= middle)
                return first;
            else if ( last >= first && last <= middle || last <= first && last >= middle )
                return last;
            else
                return middle;
        }

        template <class RandomAccessIterator, class Compare = std::greater<typename RandomAccessIterator::value_type>>
        void introsort_loop ( RandomAccessIterator first, RandomAccessIterator last, typename RandomAccessIterator::difference_type depth_limit, Compare cmp = Compare() ) {



            while ( cmp((last - first), 20) )
              {
                  if ( depth_limit == 0)
                  {
                      heapsort(first, last);
                      return;
                  }
                  else
                  {
                      depth_limit--;
                      auto cut = medianOfThree(first, last, (first + ( last - first ) / 2));
                      introsort_loop(cut, last, depth_limit, cmp);
                      last = cut;
                  }
              }
          }


    }




      template <class RandomAccessIterator, class Compare = std::greater<typename RandomAccessIterator::value_type>>
      void introsort ( RandomAccessIterator first, RandomAccessIterator last, Compare cmp = Compare() ) {

        using size_type = typename RandomAccessIterator::difference_type;
        size_type depth_limit {2*(floor(log( last - first )))};

        mylibprivate::introsort_loop(first, last, depth_limit);
        insertion_sort(first, last);
        }


}






#endif // INTROSORT_H

