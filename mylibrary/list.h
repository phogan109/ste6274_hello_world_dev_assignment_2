#ifndef LIST_H
#define LIST_H


#include <initializer_list>


namespace mylib {

  class List {
  public:
    using size_type  = std::size_t;
    using value_type = int;

    List( std::initializer_list<value_type> list );

    size_type size() const;

  private:
    size_type _size;
  }; // END class List

}  // END namespace mylib

#endif // LIST_H
