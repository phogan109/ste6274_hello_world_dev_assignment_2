#ifndef HEAPSORT_H
#define HEAPSORT_H


#include <functional>
#include <algorithm>

namespace mylib {


  template <class RandomAccessIterator, class Compare = std::less<typename RandomAccessIterator::value_type>>
  void heapsort( RandomAccessIterator first, RandomAccessIterator last, Compare cmp = Compare() )
  {
     std::make_heap(first, last);
     std::sort_heap(first, last);
  }


}



#endif // HEAPSORT_H

