% What kind of text document should we build
\documentclass[a4,10pt]{article}


% Include packages we need for different features (the less, the better)

% Clever cross-referencing
\usepackage{cleveref}

\usepackage{listings}


% Math
\usepackage{amsmath}

% Algorithms
\usepackage{algorithm}
\usepackage{algpseudocode}

% Tikz
\RequirePackage{tikz}
\usetikzlibrary{arrows,shapes,calc,through,intersections,decorations.markings,positioning}

\tikzstyle{every picture}+=[remember picture]

\RequirePackage{pgfplots}


\lstset{
    frame=single,
    breaklines=true,
    language=c++,
    basicstyle=\scriptsize}






% Set TITLE, AUTHOR and DATE
\title{A Brief Introduction to Implementing Introsort}
\author{Patrick Hogan}
\date{\today}



\begin{document}



  % Create the main title section
  \maketitle

  \begin{abstract}
     For this report a custom version of introsort, and insertion sort have been implemented in an attempt to simulate the speed of STL sort ;
     Benchmarks were then run on these sorts, along with some other sorts provided to us, including the STL sort. These benchmarks
     are represented in a graph later in the report. The results show that it is very difficult
    to emulate the speed of the STL sort, furthermore the results show just how optimized the STL sort is.
  \end{abstract}


  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %%  The main content of the report  %%
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  \section{Introduction}
  Introsort was introduced as an alternative to Quicksort in some cases. The idea behind this alternative was to find
  a solution to the worst case time of Quicksort, which is $O$($N^2$). This solution is to implement heapsort
  as a way of trying to avoid $O$($N^2$) situations during Quicksort. Heapsort's worst case time is $O$($N\log{}N$)
  and by placing Heapsort into Quicksort we avoid the Median-of-three killer sequences which cause Quicksort to be
  $O$($N^2$). In the STL libraries, the stardard sort function (std::sort) is an implementation of introsort. The purpose of this exercise was to implement and benchmark our own version of Introsort against the STL Sort. After researching
   and reviewing an article ~\cite{shell:1959} on how Introsort is implemented, I built an Introsort using, a version of Bubblesort and
   a version of Selection sort. This version of Introsort was built off of the function design and pseudo-code provided in the Musser article ~\cite{shell:1959}.
   After initial benchmarking clearly proved that when implementing Introsort with Selection sort instead of Heapsort and Bubblesort instead of Insertion sort
   the results produced do not give algorithmic orders anywhere nearly as fast as the STL sort, I substituted my own version of insertion sort and the STL version of
   Heapsort into my Introsort. The result after benchmarking this version of Introsort was much faster than the previous version but was also still much slower than
   the STL sort.

  \pagebreak

  \section{Algorithms}
  Insertion Sort Code
  \begin{lstlisting}
  template <class RandomAccessIterator, class Compare = std::less<typename RandomAccessIterator::value_type>>
  void insertion_sort(RandomAccessIterator first, RandomAccessIterator last, Compare cmp = Compare())
  {
      using size_type = typename RandomAccessIterator::difference_type;
      size_type n {last - first};
      size_type j;
      size_type x;

      for (size_type i {0}; i < n; ++i)
      {
          x = first[i];
          j = i;
          for ( ; j > 0 && x < first[j-1]; j--)
          {
              first[j] = first[j-1];
          }
          first[j] = x;
      }
  }
  \end{lstlisting}
  Introsort Code
  \begin{lstlisting}
  template <class RandomAccessIterator, class Compare = std::less<typename RandomAccessIterator::value_type>>
  void introsort_loop ( RandomAccessIterator first, RandomAccessIterator last, int depth_limit, Compare cmp = Compare() ) {

      while ( (last - first) > 20)
        {
            if ( depth_limit == 0)
            {
                heapsort(first, last);
                return;
            }
            else
            {
                depth_limit--;
                RandomAccessIterator cut = (first + ( last - first ) / 2);
                introsort_loop(cut, last, depth_limit);
                last = cut;
            }
        }
    }

    template <class RandomAccessIterator, class Compare = std::less<typename RandomAccessIterator::value_type>>
    void introsort ( RandomAccessIterator first, RandomAccessIterator last, Compare cmp = Compare() ) {

      using size_type = typename RandomAccessIterator::difference_type;
      size_type depth_limit {2*(floor(log(last - first)))};

      introsort_loop(first, last, depth_limit, cmp);
      insertion_sort(first, last);
      }
  }
  \end{lstlisting}

  \section{Benchmark set-up}
   Benchmarks were performed with the following:
     All tests were performed on a computer with the following specifications:
     \begin{enumerate}
       \item Windows 8.1
       \item 2.7GHZ Intel i7 Processor
       \item 16GB RAM
     \end{enumerate}
     For the sorting algorithms, the data sets of integers were sorted, where duration times were measured by using the high precision chrono device which is included with the STL.
     Tests were run on vectors sized with 20, 100, 1,000, 10,000, 20,000, 30,000, 40,000, 50,000, 60,000, 70,000, 80,000, 90,000, and 100,000 items respectively.
     Items in vectors were randomized using the set of integers ($-2*10^9$) to ($2*10^9$).
     Sorting algorithms were performed on the same random vector for each size iteration.
     Introsort benchmark used 20 as the value for the size threshold.


    \begin{figure}%[width=\textwidth]
      \begin{tikzpicture}
        \begin{axis}[
          xmajorgrids=false,ymajorgrids=false,
          legend pos=north west,
          width=0.6\textwidth, height=0.6\textheight,
%          reverse legend
          ]
          \addplot[black] table[x=size ,y=time, skip first n=0] {dat/benchmark_mylib_bubble.dat};
          \addplot[magenta] table[x=size ,y=time, skip first n=0] {dat/benchmark_mylib_shell.dat};
          \addplot[green] table[x=size ,y=time, skip first n=0] {dat/benchmark_mylib_selection_sort.dat};
          \addplot[orange] table[x=size ,y=time, skip first n=0] {dat/benchmark_mylib_introsort.dat};
          \addplot[blue] table[x=size ,y=time, skip first n=0] {dat/benchmark_mylib_insertionsort.dat};
          \addplot[red] table[x=size ,y=time, skip first n=0] {dat/benchmark_mylib_heapsort.dat};
          \addplot[cyan] table[x=size ,y=time, skip first n=0] {dat/benchmark_stl_sort.dat};
          \legend{
            Bubble,
            Shell,
            Selection Sort,
            Introsort,
            Insertion Sort,
            Heapsort,
            STL sort
          }
        \end{axis}
      \end{tikzpicture}
      \begin{tikzpicture}
        \begin{axis}[
          xmajorgrids=false,ymajorgrids=false,
          legend pos=north west,
          width=0.6\textwidth, height=0.6\textheight,
%          reverse legend
          ]
          \addplot[orange] table[x=size ,y=time, skip first n=0] {dat/benchmark_mylib_introsort.dat};
          \addplot[blue] table[x=size ,y=time, skip first n=0] {dat/benchmark_mylib_insertionsort.dat};
          \addplot[red] table[x=size ,y=time, skip first n=0] {dat/benchmark_mylib_heapsort.dat};
          \addplot[cyan] table[x=size ,y=time, skip first n=0] {dat/benchmark_stl_sort.dat};
          \legend{
            Introsort,
            Insertion Sort,
            Heapsort,
            STL sort
          }
        \end{axis}
      \end{tikzpicture}
      \caption{Graph representing the time(ms) of (y) it takes to sort a randomized vector of size (x)}
      \label{fig:bench_sort}
    \end{figure}



  \section{Concluding remarks}
    Introsort implemented with Selection sort and Bubblesort does not run as fast as Introsort implemented with Heapsort and Insertion Sort respectively.
    The major bottleneck in the implementation is my version of insertion sort. This could be the biggest gap between this Introsort and the STL sort.
    The STL sort is highly optimized. This is proven by the difference in benchmark results between the implementation for Introsort done in this assignment and the STL sort.
    Changing the size threshold for Introsort did not yield significant results in my implementation.
    Advancing this implementation would involve improving the Insertion sort function to be optimal.



  % Include the bibliography
  \bibliographystyle{plain}
  \bibliography{bibliography}

\end{document}
